//
//  AppDelegate.h
//  assignment
//
//  Created by Devin Marsh (201239464) on 2018-09-27.
//  Copyright © 2018 Devin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

