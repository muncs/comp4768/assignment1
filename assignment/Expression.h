//
//  Expression.h
//  assignment
//
//  Created by Devin on 2018-09-28.
//  Copyright © 2018 Devin. All rights reserved.
//

#ifndef Expression_h
#define Expression_h

@class Expression;

@interface Expression : NSObject

@property (strong, nonatomic) NSMutableArray *stack;

- (void)addTerm:(NSString *)term;
- (double)evaluate;

@end

#endif /* Expression_h */
