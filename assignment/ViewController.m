//
//  ViewController.m
//  assignment
//
//  Created by Devin Marsh (201239464) on 2018-09-27.
//  Copyright © 2018 Devin. All rights reserved.
//

#import "ViewController.h"
#import "math.h"

@interface ViewController ()

@end

@implementation ViewController

NSString *DEFAULT_VAUE = @"0";      //Default string label for equation field
NSString *operator = nil;           //Current arithmatic operation
double lhTerm, rhTerm;              //Left hand and right hand terms (the operands)
bool newInput = true;               //Used to determine if we need to 'chain' answers with a new operation, or if its a new input

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    //Initialize the starting label
    _labelEquation.text = DEFAULT_VAUE;
}

/*
 * Add a number character to the operands
 */
- (IBAction)btnNumberTapped:(UIButton *)sender {
    if (_labelEquation.text == DEFAULT_VAUE || newInput) {
        _labelEquation.text = sender.titleLabel.text;
        newInput = false;
    } else {
        _labelEquation.text = [_labelEquation.text stringByAppendingString:sender.titleLabel.text];
    }
}

/*
 * Clear the entire equation label
 */
- (IBAction)btnClearTapped:(UIButton *)sender {
    if (_labelEquation.text != DEFAULT_VAUE) {
        _labelEquation.text = DEFAULT_VAUE;
    }
}

/*
 * Delete the last character in the equation label
 */
- (IBAction)btnDeleteTapped:(UIButton *)sender {
    if (_labelEquation.text != DEFAULT_VAUE) {
        _labelEquation.text = [_labelEquation.text substringToIndex:[_labelEquation.text length] - 1];
    }
}

/*
 * Perform operation
 */
- (IBAction)btnEqualTapped:(id)sender {
    if (_labelEquation.text != DEFAULT_VAUE) {
        rhTerm = self.parseRhTerm;
        newInput = true;
        
        if ([operator isEqualToString:@"+"]) {
            _labelEquation.text = [NSString stringWithFormat:@"%f", lhTerm + rhTerm];
        } else if ([operator isEqualToString:@"-"]) {
            _labelEquation.text = [NSString stringWithFormat:@"%f", lhTerm - rhTerm];
        } else if ([operator isEqualToString:@"*"]) {
            _labelEquation.text = [NSString stringWithFormat:@"%f", lhTerm * rhTerm];
        } else if ([operator isEqualToString:@"/"]) {
            _labelEquation.text = [NSString stringWithFormat:@"%f", lhTerm / rhTerm];
        } else if ([operator isEqualToString:@"^"]) {
            _labelEquation.text = [NSString stringWithFormat:@"%f", pow(lhTerm, rhTerm)];
        }
    }
}

/*
 * Add a decimal to the current number
 */
- (IBAction)btnDecimalTapped:(UIButton *)sender {
    if ([_labelEquation.text rangeOfString:@"."].location == NSNotFound) {
        _labelEquation.text = [_labelEquation.text stringByAppendingString:sender.titleLabel.text];
    }
}

/*
 * Append an operator symbol to equation label
 */
- (IBAction)btnOperatorTapped:(UIButton *)sender {
    operator = sender.titleLabel.text;
    lhTerm = [_labelEquation.text doubleValue];
    
    //Determine if its new input or chaining from previous answer
    if (newInput) {
        newInput = false;
    }
    
    if (_labelEquation.text != DEFAULT_VAUE) {
        _labelEquation.text = [_labelEquation.text stringByAppendingFormat:@" %@ ", operator];
    }
}

/*
 * Special operation: 1/x
 */
- (IBAction)btnInverseTapped:(UIButton *)sender {
    lhTerm = [_labelEquation.text doubleValue];
    _labelEquation.text = [NSString stringWithFormat:@"%f", 1 / lhTerm];
}

/*
 * Special operation: sqrt(x)
 */
- (IBAction)btnSquareRootTapped:(UIButton *)sender {
    lhTerm = [_labelEquation.text doubleValue];
    _labelEquation.text = [NSString stringWithFormat:@"%f", sqrt(lhTerm)];
}

/*
 * Special operation: x^2
 */
- (IBAction)btnSquaredTapped:(UIButton *)sender {
    lhTerm = [_labelEquation.text doubleValue];
    _labelEquation.text = [NSString stringWithFormat:@"%f", pow(lhTerm, 2.0)];
}

/*
 * Special operation: x^3
 */
- (IBAction)btnPowThreeTapped:(UIButton *)sender {
    lhTerm = [_labelEquation.text doubleValue];
    _labelEquation.text = [NSString stringWithFormat:@"%f", pow(lhTerm, 3.0)];
}

/*
 * Special operation: x^y
 */
- (IBAction)btnXPowY:(UIButton *)sender {
    operator = @"^";
    lhTerm = [_labelEquation.text doubleValue];
    
    if (newInput) {
        newInput = false;
    }
    
    if (_labelEquation.text != DEFAULT_VAUE) {
        _labelEquation.text = [_labelEquation.text stringByAppendingFormat:@" %@ ", operator];
    }
}

/*
 * Reverse the sign of the first number
 */
- (IBAction)btnPlusMinusTapped:(UIButton *)sender {
    double val = [_labelEquation.text doubleValue];
    val *= -1;
    
    _labelEquation.text = [NSString stringWithFormat:@"%f", val];
}

/*
 * Utility function to determine right hand operand
 */
- (double)parseRhTerm {
    NSRange range = [_labelEquation.text rangeOfString:operator];
    if (range.location != NSNotFound) {
        return [[_labelEquation.text substringFromIndex:range.location + 1] doubleValue];
    } else return 0;
}
@end
