//
//  ViewController.h
//  assignment
//
//  Created by Devin Marsh (201239464) on 2018-09-27.
//  Copyright © 2018 Devin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *labelEquation;
@property (weak, nonatomic) IBOutlet UIButton *operatorPlus;
@property (weak, nonatomic) IBOutlet UIButton *operatorMinus;
@property (weak, nonatomic) IBOutlet UIButton *operatorMultiply;
@property (weak, nonatomic) IBOutlet UIButton *operatorDivide;

- (IBAction)btnNumberTapped:(UIButton *)sender;

- (IBAction)btnClearTapped:(UIButton *)sender;

- (IBAction)btnDeleteTapped:(UIButton *)sender;

- (IBAction)btnEqualTapped:(id)sender;

- (IBAction)btnDecimalTapped:(UIButton *)sender;

- (IBAction)btnOperatorTapped:(UIButton *)sender;

- (IBAction)btnInverseTapped:(UIButton *)sender;

- (IBAction)btnSquareRootTapped:(UIButton *)sender;

- (IBAction)btnSquaredTapped:(UIButton *)sender;

- (IBAction)btnPowThreeTapped:(UIButton *)sender;

- (IBAction)btnPlusMinusTapped:(UIButton *)sender;

- (IBAction)btnXPowY:(UIButton *)sender;

@end
