//
//  Expression.m
//  assignment
//
//  Created by Devin on 2018-09-28.
//  Copyright © 2018 Devin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Expression.h"

@implementation Expression

- (instancetype)init {
    _stack = [[NSMutableArray alloc] init];
    return self;
}

- (void)addTerm:(NSString *)term {
    [_stack addObject:term];
}

- (double)evaluate {
    
}

@end
