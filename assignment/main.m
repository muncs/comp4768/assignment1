//
//  main.m
//  assignment
//
//  Created by Devin Marsh (201239464) on 2018-09-27.
//  Copyright © 2018 Devin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
